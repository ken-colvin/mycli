** I wish I had a tool to anaylyze MySQL tables on the command line? **

* Wish comes true ... 

** MYCLI - Awesome auto-completion mysql command line tool **

If you know SQL then this is better then an IDE

* Follow the install steps on https://www.mycli.net/
* Put the .myclirc in your home directory
* you will now be more productive analyzing mysql

----

I used the Classic Car database in my examples.  Here are some example databases

|Database|Link|
|--------|----|
|Classic Car|http://www.mysqltutorial.org/mysql-sample-database.aspx|
|Employees|https://dev.mysql.com/doc/employee/en/employees-installation.html|
|World|https://dev.mysql.com/doc/world-setup/en/world-setup-installation.html|

----

** Create a user and database for the command line access for Classic Cars **

On the command line boot up mycli
* mycli -u root	

Enter these sql commands to create the user

```sql
-- create user
CREATE USER 'itsme'@'localhost' IDENTIFIED BY 'password';
-- allows this use to use classicars database
GRANT ALL PRIVILEGES ON classiccars.* TO 'itsme'@'localhost';
-- check to make sure they have access
show grants for 'itsme'@'localhost'
-- you should see a row output
-- GRANT ALL PRIVILEGES ON `classiccars`.* TO 'itsme'@'localhost'
-- inside mycli, source the classiccars, which creates the db and insert data
source ./classicmodels.sql
```

